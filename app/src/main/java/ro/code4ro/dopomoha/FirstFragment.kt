package ro.code4ro.dopomoha

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import ro.code4ro.dopomoha.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.code4ro.setOnClickListener{
            startActivity(Intent(ACTION_VIEW, Uri.parse("https://code4.ro")))
        }
        binding.findOutMore.setOnClickListener{
            startActivity(Intent(ACTION_VIEW, Uri.parse("https://code4.ro")))
        }
        binding.sdu.setOnClickListener{
            startActivity(Intent(ACTION_VIEW, Uri.parse("http://www.dsu.mai.gov.ro")))
        }
        binding.iom.setOnClickListener{
            startActivity(Intent(ACTION_VIEW, Uri.parse("https://romania.iom.int")))
        }
        binding.cnnr.setOnClickListener{
            startActivity(Intent(ACTION_VIEW, Uri.parse("https://www.cnrr.ro")))
        }
        binding.logo.setOnClickListener{
            startActivity(Intent(ACTION_VIEW, Uri.parse("https://dopomoha.ro/uk")))
        }

        binding.contact.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk/dopomohaapp"))
        }
        binding.faq.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk"))
        }
        binding.enter.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk/yak-uvijti-do-rumuniyi"))
        }
        binding.asylum.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk/prohannya-pro-nadannya-pritulku"))
        }
        binding.expressions.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk/rozmovnik"))
        }
        binding.besafe.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk/zalishajsya-v-bezpeci"))
        }
        binding.manage.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundleOf("url" to "https://dopomoha.ro/uk/yak-perebuvati-v-rumuniyi"))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}